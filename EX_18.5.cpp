

#include <iostream>

class Stack
{
private:

	int ArrayLength{};
	int* Array{};

public:

	Stack(int len = 5)
	{
		ArrayLength = len;
		Array = new int[ArrayLength];
	}

	~Stack() //destructor
	{
		delete[] Array;
	}
	
	void Filling()
	{
		for (int i = 0; i < ArrayLength; i++)
		{
			Array[i] = i;
		}
	}

	void Print()
	{
		for (int i = 0; i < ArrayLength; i++)
		{
			std::cout << "\n" << Array[i];
		}
	}

	int Pop()
	{
		//std::cout << "\nYour last unit of the array is " << Array[ArrayLength - 1];
		int ToPrint;
		ToPrint = Array[ArrayLength - 1];
		int CopyLength = ArrayLength - 1;
		int* Copy = new int[CopyLength];

		for (int i = 0; i < (ArrayLength-1); i++)
		{
			Copy[i] = Array[i];
			//note that other elements from ArrayLength to CopyLength - 1 are not initialized
		}

		//free unnecessary old array
		delete[] Array;

		//update fields
		ArrayLength = CopyLength;
		Array = Copy;

		return ToPrint;
	}


	void Push()
	{
		int CopyLength = ArrayLength + 1;
		int* Copy = new int[CopyLength];

		for (int i = 0; i < ArrayLength; i++)
		{
			Copy[i] = Array[i];
			//note that other elements from ArrayLength to CopyLength - 1 are not initialized
		}
		std::cout << "\nPlease enter number to be imlemented in array\n";
		int a;
		std::cin >> a;
		Copy[ArrayLength] = a;

		//free unnecessary old array
		delete[] Array;

		//update fields
		ArrayLength = CopyLength;
		Array = Copy;
	}
};


int main()
{
	int len{}; //array length
	std::cout << "\nPlease enter the length of array: "; //set user request for array length
	std::cin >> len; //save result
	Stack temp(len);
	temp.Filling();

	bool run = true;
	while (run == true)
	{
		std::cout << "\nWhat do you want to do next?\nPress numbers to execute the following commands:\n1 - print\n2 - Delete very last unit\n3 - Enchance array\n4 - exit\n"; 
		int action;
		std::cin >> action;
		if (action == 1)
		{
			temp.Print();
		}
		else
		{
			if (action == 2)
			{
				std::cout << "\nFollowing element of the array has been removed:\n" << temp.Pop() << "\n";
			}
			else
			{
				if (action == 3)
				{
					temp.Push();
				}
				else
				{
					if (action == 4)
					{
						run = false;
					}
					else
					{
						std::cout << "\nUnknown command\n";
					}
				}
				
			}
			
		}
		
	}

}